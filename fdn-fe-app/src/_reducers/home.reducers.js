import { landingPagesConstants } from "../_actions/constants";

const initState = {
  editorsChoice: [],
  loading: false,
  error: null,
  latestArticles: [],
  latestReview: [],
};

export default (state = initState, action) => {
    console.log(action);
  switch (action.type) {
    case landingPagesConstants.GET_CONTENT_HOME_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case landingPagesConstants.GET_CONTENT_HOME_SUCCESS:
      return {
        ...state,
        loading: false,
        editorsChoice: action.payload.editorsChoice,
        latestArticles: action.payload.latestArticles,
        latestReview: action.payload.latestReview,
      };
    case landingPagesConstants.GET_CONTENT_HOME_FAILURE:
      return {
        ...state,
        error: action.payload.error,
        loading: false,
      };
    default: 
      return state;
  }
};

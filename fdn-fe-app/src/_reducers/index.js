import { combineReducers } from "redux";
import homeReducer from "./home.reducers";

const rootReducer = combineReducers({
    data: homeReducer
});

export default rootReducer;
import React from "react";
import Rating from "../Rating";

const ProductCard = (props) => {
    return (     
        <div className="card product-card">
            <img
                src={props.image}
                className="card-img-top"
                alt="Hollywood Sign on The Hill"
            />
            <div className="card-body p-0">
                <Rating
                    rate={props.rate}
                    max={5}
                    reviewNumber={props.population}
                    isText={true}
                    />
                <p className="card-title mt-1">{props.title}</p>
                <p className="card-text mb-1">
                    {props.desc}
                </p>
                <p className="product-variant">{props.variant}</p>
            </div>
        </div>
    )
}

export default ProductCard;
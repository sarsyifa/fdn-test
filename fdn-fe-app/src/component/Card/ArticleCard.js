import React from "react";

const ArticleCard = (props) => {
    return (
        
        <div className="article-card">
            <img
                src={props.image}
                className="card-img-top"
                alt={props.title}
            />
            <div className="card-body p-0">
                <p className="card-title mt-3">{props.title}</p>
                <p className="card-text text-gray mb-1">
                    {props.author} | 
                    <span className="span ml-1">{props.published_at}</span>
                </p>
                
            </div>
        </div>
    )
}

export default ArticleCard;
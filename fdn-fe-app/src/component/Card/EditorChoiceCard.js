import React from "react";
import Rating from "../Rating";

const EditorChoiceCard = (props) => {
    return (
        <div data-testid="productCard" className="editor-choice-card">
            <div className="editor-profile">
                <div className="card-profile-wrapper">
                    <div className="image-wrapper">
                        <a className="card-profile-picture" href="#">
                            <img 
                                alt={props.editor}
                                src={props.profilePicture}
                                style={{width: "42px", height: "42px"}}
                                />
                        </a>
                    </div>
                    <div className="text-wrapper">
                        <p className="profile-username">{props.editor}</p>
                        <p className="profile-desc mb-0">{props.editorDesc}</p>
                    </div>
                </div>
            </div>
            <div className="card product-card">
                <img
                    src={props.image}
                    className="card-img-top"
                    alt="Hollywood Sign on The Hill"
                />
                <div className="card-body p-0">
                    <Rating
                        rate={props.rate}
                        max={5}
                        reviewNumber={props.population}
                        isText={true}
                        />
                    <p className="card-title mt-1">{props.title}</p>
                    <p className="card-text mb-1">
                        {props.desc}
                    </p>
                    <p className="product-variant">{props.variant}</p>
                </div>
            </div>
        </div>
    )
}

export default EditorChoiceCard;
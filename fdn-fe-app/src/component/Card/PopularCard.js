import React from "react";

const PopularCard = (props) => {
    return (
        
        <div className="popular-card">
            
            <div className="avatar-wrapper">
                <img
                    src={props.image}
                    className="card-img-top"
                    alt={props.title}
                />
            </div>
            <h5>{props.title}</h5>
            <div className="row">
                <div className="col-4">
                    <div className="dropdown">
                        <button className="btn btn-link dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i className="fa fa-user"></i>
                        </button>
                        <div className="dropdown-content">
                            <a className="dropdown-item" href="#">Action</a>
                            <a className="dropdown-item" href="#">Another action</a>
                            <a className="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="dropdown">
                        <button className="btn btn-link dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                            <i className="fa fa-list-ul"></i>
                        </button>
                        <div className="dropdown-content">
                            <a className="dropdown-item" href="#">Action</a>
                            <a className="dropdown-item" href="#">Another action</a>
                            <a className="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                <div className="col-4">
                    <div className="dropdown">
                        <button className="btn btn-link dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="true">
                            <i className="fa fa-commenting-o"></i>
                        </button>
                        <div className="dropdown-content">
                            <a className="dropdown-item" href="#">Action</a>
                            <a className="dropdown-item" href="#">Another action</a>
                            <a className="dropdown-item" href="#">Something else here</a>
                        </div>
                    </div>
                </div>
                
            </div>
            <p className="card-text">
                {props.desc}
            </p>
        </div>
    )
}

export default PopularCard;
import React from "react";
import Rating from "../Rating";

const ReviewCard = (props) => {
    return (
        <div data-testid="reviewCard" className="review-card-container">
            <div className="review-card">
                <div className="card-header">
                    <div className="product-image-wrapper">
                        <img
                            alt={props.productName}
                            src={props.image}
                            style={{width: "65px", height: "79px"}}
                            />
                    </div>
                    <div className="product-header">
                        <p className="card-title mt-1">{props.productName}</p>
                        <p className="card-text mb-1">{props.productDesc}</p>
                    </div>
                    
                </div> 
                <div className="card-body p-0">
                    <div className="review-container">
                        <div className="rating-container">
                            <Rating
                                rate={props.rate}
                                max={5}
                                />
                        </div>
                        <p className="published-time">{props.published_at}</p>
                        
                    </div>
                    <p className="comment">{props.comment}</p>
                </div>
            </div>
            <div className="reviewer-profile-container">
                <img 
                    alt={props.reviewer}
                    className="reviewer-profile"
                    src={props.profilePicture}
                    style={{width: "42px", height: "42px"}}
                    />
            </div>
            <p className="reviewer-username">{props.reviewer}</p>
            <small className="reviewer-desc">{props.reviewerDesc.join(", ")}</small>
        </div>
    )
}

export default ReviewCard;
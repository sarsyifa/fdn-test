import React from "react";

const Rating = (props) => {
    var rate = props.rate;
    var filled = Math.floor(rate);
    var outline = props.max - filled;
    return (
        <div className="rating-section d-flex mt-3 mb-3">
            {props.isText && <span className="rating-text">{rate}</span>}
            <ul className="rating">
                { Array.from({length: filled}, (v, i) => 
                    <li key={`${i}-filled`}>
                        <i className="fa fa-star fa-sm text-fd"></i>
                    </li>
                    )   
                }
                { Array.from({length: outline}, (v, i) => 
                    <li key={`${i}`}>
                        <i className="fa fa-star-o fa-sm text-fd"></i>
                    </li>
                    )   
                }
            </ul>
            {props.reviewNumber && <span className="review-number">({props.reviewNumber})</span>}
        </div>
    )
}

export default Rating;
import React from "react";
import { Link } from "react-router-dom";

const Section = (props) => {
    return (
        
        <>  
            <div className="panel">
                <div className="section-title">
                    <h4 className="title">{props.title}</h4>
                    <p className="subtitle">{props.subtitle}</p>
                </div>
                <Link to={props.linkTo}>
                    <div className="link-title">See more <i className="fa fa-chevron-right"></i></div>
                </Link>
            </div>
            <div className="section-items">
                {props.children}
            </div>
        </>
    )
}

export default Section;
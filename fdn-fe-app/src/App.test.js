import { render, screen, cleanup, getByTestId } from '@testing-library/react';
import ReactDOM from 'react-dom';
import App from './App';
import EditorChoiceCard from './component/Card/EditorChoiceCard';
import '@testing-library/jest-dom/extend-expect';
import renderer from 'react-test-renderer';

afterEach(cleanup);

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/login/i);
  expect(linkElement).toBeInTheDocument();
});

test('renders product card element', () => {
  const divElement = document.createElement("div");
  ReactDOM.render(<EditorChoiceCard></EditorChoiceCard>, divElement);
});

test('renders editors choice correctly', () => {
  const element = render(<EditorChoiceCard title="Female Daily"></EditorChoiceCard>).getByTestId('productCard');
  expect(element).toBeInTheDocument("Female Daily");
});
const brands = [
    {
        name: "Nivea",
        image: "https://image.femaledaily.com/dyn/250/images/brands-pics/brands_1552546717_Nivea_Logo_800x800.png"
    },
    {
        name: "The Ordinary",
        image: "https://image.femaledaily.com/dyn/250/images/brands-pics/brands_1628574463_0ba4c8f16e_800x800.png"
    },
    {
        name: "The Body Shop",
        image: "https://image.femaledaily.com/dyn/250/images/brands-pics/brands_1542081653_tbs_800x800.png"
    },
    {
        name: "SK-II",
        image: "https://image.femaledaily.com/dyn/250/images/brands-pics/brands_1455261593_SK_II_logo_800x800.png"
    },
    {
        name: "Maybelline",
        image: "https://image.femaledaily.com/dyn/250/images/brands-pics/brands_1571201621_logo_maybe_800x800.png"
    },
    {
        name: "Innisfree",
        image: "https://image.femaledaily.com/dyn/250/images/brands-pics/brands_1552634273_Innisfree__800x800.png"
    }
    
]

export default brands
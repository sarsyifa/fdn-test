
import React from 'react';
import './assets/css/App.css';
import { Provider } from 'react-redux';
import store from './_store';
import { BrowserRouter as Router} from 'react-router-dom';
import Home from './pages/Home';

function App() {

  return (
    <Provider store={store}>
      <Router>
        <Home/>
      </Router>
    </Provider>
  );
}

export default App;

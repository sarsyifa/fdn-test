
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { Link } from 'react-router-dom';
import { getContent } from '../.././_actions';
import EditorChoiceCard from '../.././component/Card/EditorChoiceCard';
import ArticleCard from '../.././component/Card/ArticleCard';
import ReviewCard from '../.././component/Card/ReviewCard';
import Carousel from '../.././component/Carousel';
import Section from '../.././component/Section';
import PopularCard from '../../component/Card/PopularCard';
import ProductCard from '../../component/Card/ProductCard';
import brands from '../../assets/dummys/brands';

const Home = () =>  {
  const contents = useSelector((state) => state.data);
  const dispatch = useDispatch();

  const [search, setSearch] = useState("");

  useEffect(() => {
    dispatch(getContent());
  }, [dispatch]);

  const handleSearch = () => {
    console.log("search", search);
    contents.latestArticles.filter((data) => {
      if (search == "") return data;
      else if (
        data.title.includes(search.toLowerCase())
      ) {
        return data;
      }
    })
  
  };


  return (

    <div className="App">
        <header>
            <nav className="navbar">
            <div className="container navbar-wrapper m-0">
                
                <div className="logo-wrapper">
                    <span className="mr-3">
                        <i className="fa fa-bars"></i>
                    </span>
                    <a className="mr-0" href="https://femaledaily.com/">
                        <img
                            src="https://app.femaledaily.com/wp-content/uploads/2018/07/FD-Logo-Pink.png"
                            height="35"
                            alt="Female Daily"
                            loading="lazy"
                        />
                    </a>
                </div>
                <div className="search-input-wrapper">
                    <div className="input-group search-input">
                        <button className="btn search-btn " type="button" onClick={handleSearch}>
                        <i className="fa fa-search" aria-hidden="true"></i>
                        </button>
                        <input 
                        type="text" 
                        className="form-control" 
                        placeholder="Search products, articles, topics, brands, etc"
                        name="searchKey"
                        value={search}
                        onChange={(e) => setSearch(e.target.value)}
                        style={{borderLeft: "0px"}}/>
                    </div>
                </div>
                <div className="button-wrapper">
                    <button className="btn login">Login</button>
                </div>

            </div>
            </nav>
            <div className="topnav">
                <a href="#skincare">Skincare</a>
                <a href="#makeup">Make Up</a>
                <a href="#body">Body</a>
                <a href="#hair">Hair</a>
                <a href="#fragnance">Fragnance</a>
                <a href="#nails">Nails</a>
                <a href="#tools">Tools</a>
                <a href="#brands">Brands</a>
            </div>
        </header>

        <div className="container">
          <div className="row justify-content-center">
            <div className="top-frame">
            </div>
            <div className="billboard">
            </div>
          </div>

          <Section 
            title={"Editor's Choice"}
            subtitle={"Curated with love"}
            linkTo={"/editors-choice"}
            >
              {contents.editorsChoice.length > 0 ? contents.editorsChoice.map((item, index) => (
                <div className="col-ec">
                <div className="editor-card-container">
                  <EditorChoiceCard 
                    profilePicture={"https://image.femaledaily.com/dyn/80/images/user-pics/profile_1626102326331.jpg"}
                    profilePicture={"https://cdn.pixabay.com/photo/2021/01/04/10/41/icon-5887126_960_720.png"}
                    editor={item.editor}
                    editorDesc={item.role}
                    // image={item.product.image}
                    image={"https://image.femaledaily.com/dyn/500/images/prod-pics/product_1558327814_YOU_MAKEUP_800x800.png"}
                    title={item.product.name}
                    desc={item.product.description}
                    rate={item.product.rating}
                    population={7}
                    variant="Rosy Beige"
                    />
                </div>
                </div>
              )) : <div className="col-m-10 col-10">
                      Empty data
                  </div>}
          </Section>
          
        </div>

        <div className="section-recommendation">
          <img
          src={"https://editorial.femaledaily.com/wp-content/uploads/2021/12/FD-Editorial-Sponsored-Post-Web-Banner-03.jpg"}
          height={"320vh"}
          width={"100%"}
          style={{objectFit: "cover"}}
          />
        </div>
        
        <div className="container">
          <div className="row justify-content-center">
            <div className="billboard">
            </div>
          </div>
          
          <Section 
            title={"Latest Articles"}
            subtitle={"So you can make better purchase decision"}
            linkTo={"/articles"}
            >
              {contents.latestArticles.length > 0 ? contents.latestArticles.map((article, index) => (
                <div className="col-m-4 col-4 col-s-12">
                <ArticleCard
                  image={article.image}
                  title={article.title}
                  author={article.author}
                  published_at={article.published_at}
                  />
              </div>
            )) : <div className="col-m-10 col-10">
                  Empty data
                </div>}
          </Section>

          <Section 
            title={"Latest Reviews"}
            subtitle={"So you can make better purchase decision"}
            linkTo={"/reviews"}
            >
              <Carousel show={2} infiniteLoop>
                  { contents.latestReview.length > 0 ? contents.latestReview.map((review, index) => (
                    <div className="col-m-4 col-4 col-s-12" style={{display: "flex"}}>
                      <ReviewCard
                        // image={review.product.image}
                        image={"https://image.femaledaily.com/dyn/640/images/prod-pics/product_1525323944_Tonymoly_D_800x800.jpg"}
                        productName={review.product.name}
                        productDesc={review.product.desc}
                        rate={review.star}
                        published_at={"2 hours ago"}
                        comment={review.comment}
                        profilePicture={"https://www.maxpixel.net/static/photo/1x/Icon-Female-Avatar-Female-Icon-Red-Icon-Avatar-6007530.png"}
                        reviewer={review.user}
                        reviewerDesc={review.profile}

                        />
                    </div>
                  )) : <div className="col-m-10 col-10">
                        Empty data
                      </div>}
                </Carousel>
          </Section>

          <Section 
            title={"Popular Groups"}
            subtitle={"Where the beuty TALK are"}
            linkTo={"/popular-groups"}
            >
              {contents.latestArticles.length > 0 ? contents.latestArticles.slice(0,4).map((article, index) => (
                <div className="col-m-3 col-3 col-s-12">
                <PopularCard
                  image={"https://www.maxpixel.net/static/photo/1x/Icon-Female-Avatar-Female-Icon-Red-Icon-Avatar-6007530.png"}
                  title={"Embrace the Curl"}
                  desc={"May your curls pop and never stop!"}
                  />
              </div>
            )) : <div className="col-m-10 col-10">
                  Empty data
                </div>}
          </Section>

          <Section 
            title={"Latest Videos"}
            subtitle={"Watch and learn, ladies"}
            linkTo={"/videos"}
            >
                <div className="row">
                    <div className="col-8 ">
                        <div className="dummy-v1    ">
                        </div>
                    </div>
                    <div className="col-4">
                        <div className="row">
                            <div className="dummy-v2">
                            
                            </div>
                        </div>
                        <div className="row">
                            <div className="dummy-v2">
                            
                            </div>
                        </div>
                        
                    </div>
                </div>
               
          </Section>

          <Section 
            title={"Top Brands"}
            subtitle={"We all know and love"}
            linkTo={"/brands"}
            >
            {brands.map((brand, idx) => (
                <Link to={"#brand/id"}>
                <div className="col-2 col-s-12" key={idx}>
                    <img
                        className="brand-img"
                        src={brand.image}
                        alt={brand.name}
                        />
                    
                </div>
                </Link>
            ))}
          </Section>

          <Section 
            title={"Trending This Week"}
            subtitle={"See our weekly most reviewed products"}
            linkTo={"/products"}
            >  
              {contents.editorsChoice.length > 0 ? contents.editorsChoice.map((item, index) => (
                <div className="col-ec">
                <div className="editor-card-container">
                  <ProductCard
                    // image={item.product.image}
                    image={"https://image.femaledaily.com/dyn/500/images/prod-pics/product_1558327814_YOU_MAKEUP_800x800.png"}
                    title={item.product.name}
                    desc={item.product.description}
                    rate={item.product.rating}
                    variant="Rosy Beige"
                    />
                </div>
                </div>
              )) : <div className="col-m-10 col-10">
                      Empty data
                  </div>}
          </Section>

        </div>
        <hr/>
        <div className="container">
            <footer className="footer">
                <div className="row">
                    <div className="col-9">
                        <div className="col-3">
                            <ul className="nav-wrapper">
                                <li className="nav-item">
                                    <Link to="">About Us</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="">Feedback</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="">Content</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-3">
                            <ul className="nav-wrapper">
                                <li className="nav-item">
                                    <Link to="">Term & Conditions</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="">Privacy Policy</Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="">Help</Link>
                                </li>
                            </ul>
                        </div>
                        <div className="col-3">
                            <ul className="nav-wrapper">
                                <li className="nav-item">
                                    <Link to=""><span>Awards</span></Link>
                                </li>
                                <li className="nav-item">
                                    <Link to="">Newsletter</Link>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div className="col-3">
                        <div className="row">
                            <h6 className="nav-item ml-4">
                                Download Our Mobile App
                            </h6>
                            <div className="row download-mobile">
                                <a href="https://apps.apple.com/id/app/female-daily-beauty-review/id1160742672?l=id">
                                    <img src="https://www.freepnglogos.com/uploads/app-store-logo-png/download-on-the-app-store-logo-png-23.png" width="170" alt="female daily play store" />
                                </a>
                                <a href="https://play.google.com/store/apps/details?id=com.fdbr.android&hl=en_GB">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSHW_EGJYpX32dyUcuF0q7oG1TclTD-nE8zxgvfHnHgxHEuPCho0v7u4_7_Ig6PqcLIXW8&usqp=CAU" width="100" alt="female daily play store" />
                                </a>
                            </div>
                        </div>
                        
                    </div>

                    <div className="row">
                        <div className="col-9">
                            <a className="ml-4" href="https://femaledaily.com/">
                                <img
                                    src="https://app.femaledaily.com/wp-content/uploads/2018/07/FD-Logo-Pink.png"
                                    height="35"
                                    alt="Female Daily"
                                    loading="lazy"
                                />
                            </a>
                        </div>
                        <div className="col-9">
                       

                        </div>
                    </div>

                </div>
            </footer>
        </div>

        <div className="container">
            <div className="row justify-content-center">
                <div className="bottom-frame">
                </div>
            </div>
        </div>

    </div>
  );
}

export default Home;

import axios from "../helpers/axios";
import { landingPagesConstants } from "./constants";

export const getContent = () => {
  return async (dispatch) => {
    dispatch({ type: landingPagesConstants.GET_CONTENT_HOME_REQUEST });
    const res = await axios.get(
      `/0.1/wp`, 
    );
    if (res.status === 200) {
    //   const { editor's choice  , totalData, sendBird, parentId } = res.data;
      dispatch({
        type: landingPagesConstants.GET_CONTENT_HOME_SUCCESS,
        payload: { editorsChoice: res.data["editor's choice"], latestArticles: res.data["latest articles"], latestReview:  res.data["latest review"] },
      });
    } else {
      dispatch({
        type: landingPagesConstants.GET_CONTENT_HOME_FAILURE,
        payload: { error: res.data.error },
      });
    }
  };
};
Built with React.js and redux. 

### Usage
Deployment: [fdn-fe-app.herokuapp.com](https://fdn-fe-app.herokuapp.com/)

Repository: [Gitlab](https://gitlab.com/sarsyifa/fdn-test)

## Run fdn-fe-app

1. Clone the repository to the local machine: `git clone https://gitlab.com/sarsyifa/fdn-test.git`

2. Go to `fdn-fe-app` folder: `cd fdn-fe-app`

3. Install dependencies
run the following command: `npm install`

4. Run using `npm start`
This will runs the app in your local machine.
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Run the test

Execute `npm test` or `npm run test` to launches the test runner, then type `a` to run the test
